import dolfin as df
import numpy as np
def finddof(point, V):
    """Find the degree(s) of freedom which is closest to point"""
    mesh = V.mesh()
    bbtree = df.BoundingBoxTree()
    bbtree.build(mesh)
    cell_id = bbtree.compute_closest_entity(df.Point(*point))[0]
    dofmap = V.dofmap()
    dist = np.inf
    coords = dofmap.tabulate_coordinates(df.Cell(mesh,cell_id))
    dofs = dofmap.cell_dofs(cell_id)
    rvals = []
    npoint = point
    for i,coord in enumerate(coords):
        ndist = np.linalg.norm(coord-npoint)
        if ndist < dist:
            dist = ndist
            npoint = coord
    for i,coord in enumerate(coords):
        if (coord == npoint).all():
            rvals.append(dofs[i])
    return rvals


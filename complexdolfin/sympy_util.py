#encoding:utf-8
# Tuomas Airaksinen 2013
# Licenced under LGPL

import sympy as sp

from s2uprinter import S2UPrinter

d_syms = list(sp.symbols("dP,dc,dS,dx,ds",real=True))

#These must also work with argument, i.e. for example ds(1). 
#We define also real-valued functions with same names. 

dp_syms = [sp.Function(i.name) for i in d_syms]
for i in dp_syms: i.is_real=True 
#This actually makes re(ds(1))==ds(1) etc.!

#These lists (d_syms and dp_syms) are used in printer
#to place these integration measures to the end of expressions.

dP,dc,dS,dx,ds = d_syms

def SymPy2UFL_str(expr,lastargs=[]):
    expr_expand = (sp.re(expr)).expand(complex=True)

    printer = S2UPrinter()
    if len(lastargs)>0:
        printer.lastargs=lastargs
    else:
        printer.lastargs = d_syms + dp_syms
    expr_expand = printer.doprint(expr_expand)
    return printer._str(expr_expand)

def SymPy2UFL_str_re_im(expr,lastargs=[]):
    re_expr_expand = (sp.re(expr)).expand(complex=True)
    im_expr_expand = (sp.im(expr)).expand(complex=True)

    printer = S2UPrinter()
    if len(lastargs)>0:
        printer.lastargs=lastargs
    else:
        printer.lastargs = d_syms + dp_syms
    re_expr_expand = printer.doprint(re_expr_expand)
    im_expr_expand = printer.doprint(im_expr_expand)
    return printer._str(re_expr_expand), printer._str(im_expr_expand)

# We must define some linear algebra related functions, 
# that are missing in SymPy. Note, that we are interested
# only in real and imaginary part of these functions.
#
#TODO: Several UFL functions are still missing  
#    sympy matrices & vectors 
#     (do not use, just use symbols for them and associate
#     UFL representation to the symbol)
#
#    indices: i,j,k,l,p,q,r,s
#    unit_vector, unit_vectors,
#    unit_matrix, unit_matrices
#
#    perp,  inv, 
#    det <- 
#    cofac <- cofactors
#    transpose, tr, diag, diag_vector,
#    dev, skew, sym
#
#    elem_mult, elem_div, elem_pow, elem_op
#
#    variable, 
#    
#    Dx, Dn, curl, rot
#
#    atan_2, <- atan2
#
#    bessel_J, bessel_Y, bessel_I, bessel_K <- besselj,bessely,besseli,besselk
#
#    jump, avg, v('+'), v('-'), cell_avg, facet_avg
#
#    eq, ne, le, ge, lt, gt,
#
#    rhs, lhs, system, functional,
#    replace, adjoint, action, energy_norm,
#    sensitivity_rhs, derivative

class grad(sp.Function):
    nargs = 1
    @classmethod
    def eval(cls,x):
        if x.is_Number:
            if x is sp.S.Zero:
                return sp.S.Zero
    
    def _eval_is_real(self):
        return self.args[0].is_real

    def as_real_imag(self, deep=True, **hints):
        re1,im1 = self.args[0].as_real_imag()
        _grad = self.__class__
        return (_grad(re1), _grad(im1))

class nabla_grad(grad):
    pass

class nabla_div(grad):
    pass

class div(grad):
    pass

class curl(grad):
    pass

class rot(grad):
    pass

class inner(sp.Function):
    nargs = 2
    @classmethod
    def eval(cls,x1,x2):
        if x1.is_Number:
            if x1 is sp.S.Zero:
                return sp.S.Zero
        if x2.is_Number:
            if x2 is sp.S.Zero:
                return sp.S.Zero
    
    def _eval_is_real(self):
        return self.args[0].is_real and self.args[1].is_real

    def as_real_imag(self, deep=True, **hints):
        re1,im1 = self.args[0].as_real_imag()
        re2,im2 = self.args[1].as_real_imag()
        _inner = self.__class__
        return (_inner(re1,re2)-_inner(im1,im2), _inner(re1,im2)+_inner(re2,im1))

class dot(inner):
    pass

class outer(inner):
    pass

class cross(inner):
    pass
